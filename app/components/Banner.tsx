import primacare from "/primacare.svg";
import idai from "/partner/idai.png";
import kemenkes from "/partner/kemenkes.svg";

const Banner = () => {
  return (
    <div
      className="relative col-span-3 rounded-2xl bg-primary-500 bg-contain bg-bottom bg-no-repeat p-[50px]"
      style={{
        backgroundImage: `url('/banner.png')`,
      }}
    >
      {/* primaku logo */}
      <img src={primacare} alt="logo" width={166} height={36} />

      <h1 className="mt-14 text-[32px] font-semibold text-primary-700">Healthcare Management Solutions</h1>

      <h2 className="text-[18px] text-primary-700">
        Layanan Sistem Informasi Manajemen Terintegrasi dalam <br /> mengelola dan mengembangkan Fasilitas Pelayanan{" "}
        <br /> Kesehatan Anda.
      </h2>

      <h2 className="mb-3 mt-[75px] text-sm">Mitra resmi dari</h2>

      {/* partner logo */}
      <div className="box-equal gap-[38px]">
        <img src={idai} alt="idai" width={40} height={48} />
        <img src={kemenkes} alt="kemenkes" width={79} height={36} />
      </div>
    </div>
  );
};

export default Banner;
