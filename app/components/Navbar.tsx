import { Popover, PopoverContent, PopoverTrigger } from "@nextui-org/popover";
import { Button } from "@nextui-org/button";
import { FiChevronDown } from "react-icons/fi";
import { Spinner } from "@nextui-org/react";
import { IoNotifications } from "react-icons/io5";
import PrivateImage from "./PrivateImage";

export default function Navbar({ clinicImageUrl = "", userImageUrl = "", userName = "", userEmail = "" }) {
  return (
    <nav id="auth-navbar" className="sticky top-0 z-30 border-b border-outline-100 bg-white shadow-primaku1 ">
      <div className="container m-auto flex min-h-[82px] items-center justify-between">
        {clinicImageUrl ? (
          <PrivateImage
            fallback={<Spinner color="white" />}
            className="aspect-video max-h-20 w-auto rounded-none bg-transparent"
            classNames={{ img: "object-contain" }}
            imageUrl={clinicImageUrl}
          />
        ) : (
          <img src="/primacare.svg" alt="logo" width={166} height={36} />
        )}

        <div className="flex items-center gap-2">
          <Button isIconOnly className="rounded-full" variant="light">
            <IoNotifications color="#8A92A6" size={20} />
          </Button>
          {/* <img src={NotificationSvg} alt="notification" width={24} height={24} /> */}

          <div className="flex items-center gap-2">
            <PrivateImage className="w-10 rounded-full" imageUrl={userImageUrl} />
            <div>
              <h2 className="font-bold">{userName}</h2>
              <p className="text-sm text-neutral-400">{userEmail}</p>
            </div>
            <Popover placement="bottom">
              <PopoverTrigger>
                <Button isIconOnly size="sm" variant="light" className="flex items-center gap-2">
                  <FiChevronDown size={15} color="grey" />
                </Button>
              </PopoverTrigger>
              <PopoverContent className="rounded p-0">
                <form method="post" action="/logout">
                  <Button variant="light" type="submit" className="rounded">
                    Logout
                  </Button>
                </form>
              </PopoverContent>
            </Popover>
          </div>
        </div>
      </div>
    </nav>
  );
}
