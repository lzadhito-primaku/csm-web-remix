import { Avatar, AvatarProps, cn } from "@nextui-org/react";
import { useQuery } from "react-query";

interface Props extends AvatarProps {
  imageUrl?: string;
  className?: string;
}
export default function PrivateImage({ imageUrl = "", className = "", ...avatarProps }: Props) {
  const { data = "" } = useQuery({
    enabled: !!imageUrl,
    queryKey: ["private-image", imageUrl],
    queryFn: () => fetch(`/private-image${imageUrl}`).then((resp) => resp.json()),
  });

  return <Avatar src={data} className={cn(["aspect-square h-auto w-48 rounded", className])} {...avatarProps} />;
}
