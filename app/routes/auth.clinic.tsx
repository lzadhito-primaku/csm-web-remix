import { type ActionFunctionArgs } from "@remix-run/node"; // or cloudflare/deno
import { Button, Input } from "@nextui-org/react";
import Banner from "~/components/Banner";
import { createUserSession } from "~/session.server";

export const action = async ({ request }: ActionFunctionArgs) => {
  const formData = await request.formData();
  const clinicId = (formData.get("clinicId") as string) || "";
  const email = (formData.get("email") as string) || "";
  const password = (formData.get("password") as string) || "";

  try {
    const resp = await fetch(`${process.env.BASE_URL_API}/auth/clinic-login`, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/json",
        Authorization: `Basic ${btoa(`cms_web:super_secret`)}`,
      }),
      body: JSON.stringify({ clinicId, email, password }),
    });
    const { data, error } = await resp.json();

    return createUserSession({ request, data });
  } catch (error: any) {
    return { success: false, status: 400, error: error?.message };
  }
};

export default function ClinicLoginPage() {
  return (
    <main className="inset-4 grid grid-cols-7 p-4 min-h-screen">
      <Banner />
      <form method="post" className="col-span-4 my-auto sm:px-16 xl:px-64" action="/auth/clinic">
        <h2 className="mb-12 text-2xl font-bold">Masuk</h2>
        <div className="grid gap-6">
          <Input
            name="clinicId"
            variant="bordered"
            classNames={{ label: "font-bold text-md" }}
            labelPlacement="outside"
            label="ID Clinic"
            placeholder="Masukkan ID Klinik"
          />
          <Input
            name="email"
            variant="bordered"
            classNames={{ label: "font-bold text-md" }}
            labelPlacement="outside"
            label="Email"
            placeholder="Masukkan email yang terdaftar"
          />
          <Input
            name="password"
            variant="bordered"
            classNames={{ label: "font-bold text-md" }}
            labelPlacement="outside"
            label="Kata Sandi"
            placeholder="Masukkan kata sandi"
          />
          <Button color="primary" className="font-bold" type="submit">
            Masuk
          </Button>
        </div>
      </form>
    </main>
  );
}
