import { LoaderFunctionArgs, json } from "@remix-run/node";
import { useLoaderData } from "@remix-run/react";
import { getSession } from "~/session.server";

export async function loader({ request }: LoaderFunctionArgs) {
  const session = await getSession(request);

  const { data } = await fetch(`${process.env.BASE_URL_API}/primaku/clinics/${session.get("clinicUuid")}`, {
    headers: new Headers({ Authorization: `Bearer ${session.get("authToken")}` }),
  }).then((res) => res.json());
  return json(data);
}

export default function ClinicProfilePage() {
  const data = useLoaderData();

  return <div>{JSON.stringify(data)}</div>;
}
