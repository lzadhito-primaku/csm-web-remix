import { LoaderFunctionArgs, json } from "@remix-run/node";
import { useLoaderData } from "@remix-run/react";
import { getSession } from "~/session.server";

export async function loader({ request }: LoaderFunctionArgs) {
  const session = await getSession(request);
  const banks = await fetch(`${process.env.BASE_URL_API}/lov/banks?isActive=true`, {
    headers: new Headers({ Authorization: `Bearer ${session.get("authToken")}` }),
  }).then((res) => res.json());

  return json(banks);
}

export default function VendorListPage() {
  const banks = useLoaderData();

  return <div>{JSON.stringify(banks)}</div>;
}
