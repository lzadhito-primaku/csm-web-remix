import { Button } from "@nextui-org/react";
import { LoaderFunctionArgs, json } from "@remix-run/node";
import { Link, useLoaderData } from "@remix-run/react";
import { FiPlus } from "react-icons/fi";
import { getSession } from "~/session.server";

export async function loader({ request }: LoaderFunctionArgs) {
  const session = await getSession(request);
  const vendors = await fetch(`${process.env.BASE_URL_API}/clinics/${session.get("clinicUuid")}/vendors`, {
    headers: new Headers({ Authorization: `Bearer ${session.get("authToken")}` }),
  }).then((res) => res.json());

  return json(vendors);
}

export default function VendorListPage() {
  const vendors = useLoaderData();

  return (
    <div>
      <div>
        <Button
          as={Link}
          to="/clinic/master-data/vendor/create"
          color="primary"
          variant="bordered"
          startContent={<FiPlus />}
          className="min-w-fit"
        >
          Tambah Vendor
        </Button>
      </div>
      <p>{JSON.stringify(vendors)}</p>
    </div>
  );
}
