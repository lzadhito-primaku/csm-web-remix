import { Button } from "@nextui-org/react";
import { LoaderFunctionArgs, json } from "@remix-run/node";
import { Link, Outlet, useLoaderData } from "@remix-run/react";
import Navbar from "~/components/Navbar";
import { getSession } from "~/session.server";

export async function loader({ request }: LoaderFunctionArgs) {
  const session = await getSession(request);
  const { data: clinicDetail } = await fetch(
    `${process.env.BASE_URL_API}/primaku/clinics/${session.get("clinicUuid")}`,
    {
      headers: new Headers({ Authorization: `Bearer ${session.get("authToken")}` }),
    }
  ).then((res) => res.json());

  return json({
    clinicImageUrl: clinicDetail?.imageUrl,
    userName: session.get("userName"),
    userEmail: session.get("userEmail"),
    userImageUrl: session.get("userImageUrl"),
  });
}

export default function Clinic() {
  const data = useLoaderData();
  return (
    <>
      <Navbar {...data} />
      <nav className="flex gap-2 z-10 relative">
        <Button as={Link} to="/clinic/master-data/profile">
          Profile
        </Button>
        <Button as={Link} to="/clinic/master-data/vendor">
          Vendor
        </Button>
      </nav>
      <div className="absolute top-0 pt-32 h-screen w-screen bg-neutral-100">
        <Outlet />
      </div>
    </>
  );
}
