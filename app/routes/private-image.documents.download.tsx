import { LoaderFunctionArgs, json } from "@remix-run/node";
import { getSession } from "~/session.server";

export async function loader({ request }: LoaderFunctionArgs) {
  const { searchParams } = new URL(request.url);
  const session = await getSession(request);
  const res = await fetch(`${process.env.BASE_URL_API}/documents/download?${searchParams.toString()}`, {
    headers: { Authorization: `Bearer ${session.get("authToken")}` },
  });
  const blob = await res.blob();
  const arrayBuffer = await blob.arrayBuffer();
  const data = btoa(new Uint8Array(arrayBuffer).reduce((data, byte) => data + String.fromCharCode(byte), ""));

  return json(`data:${blob.type};base64,${data}`);
}
