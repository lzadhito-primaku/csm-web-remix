import { createCookieSessionStorage, redirect } from "@remix-run/node";

type SessionData = {
  authToken: string;
  clinicUuid: string;
  loginType: string;
  clinicUserRole: string;
  userName: string;
  clinicName: string;
  userImageUrl: string;
  clinicImageUrl: string;
  userEmail: string;
  clinicAddress: string;
  expiration: string;
};

type SessionFlashData = {
  error: string;
};

const sessionStorage = createCookieSessionStorage<SessionData, SessionFlashData>({
  cookie: {
    name: "__session",
  },
});

export async function getSession(request: Request) {
  const cookie = request.headers.get("Cookie");
  return sessionStorage.getSession(cookie);
}

export async function logout(request: Request) {
  const session = await getSession(request);
  return redirect("/auth/clinic", {
    headers: {
      "Set-Cookie": await sessionStorage.destroySession(session),
    },
  });
}

export async function createUserSession({ request, data }: { request: Request; data: any }) {
  const session = await getSession(request);
  session.set("authToken", data.accessToken);
  session.set("clinicUuid", data.clinicUuid);
  session.set("loginType", "clinic");
  session.set("clinicUserRole", data.roleName);
  session.set("userName", data.userName);
  session.set("clinicName", data.clinicName);
  session.set("userImageUrl", data.userImageUrl);
  session.set("clinicImageUrl", data.clinicImageUrl);
  session.set("userEmail", data.userEmail);
  session.set("clinicAddress", JSON.stringify(data.clinicAddress));

  return redirect("/clinic/master-data/profile", {
    headers: {
      "Set-Cookie": await sessionStorage.commitSession(session, {
        maxAge: 60 * data.expireInMinutes,
      }),
    },
  });
}
