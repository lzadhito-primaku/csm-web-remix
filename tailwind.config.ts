import type { Config } from "tailwindcss";
const { nextui } = require("@nextui-org/react");

const config: Config = {
  mode: "jit",
  content: ["./app/**/*.{js,jsx,ts,tsx}", "./node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}"],
  theme: {
    fontFamily: {
      sans: ["Karla"],
    },
    extend: {
      transitionProperty: {
        "max-height": "max-height",
      },
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic": "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      boxShadow: {
        primaku1: "0px 1px 2px 0px rgba(0, 0, 0, 0.05)",
      },
      fontSize: {
        "sm-regular": "13px", // Example custom size
        xss: "10px",
      },
      spacing: {
        "17": "4.375rem",
      },
    },
  },
  darkMode: "class",
  plugins: [
    nextui({
      layout: {
        radius: {
          small: "6px",
          medium: "8px", // (default)
          large: "12px",
        },
        boxShadow: {
          small: "0 1px 2px 0 rgb(0 0 0 / 0.05)",
          medium: "0 1px 3px 0 rgb(0 0 0 / 0.1), 0 1px 2px -1px rgb(0 0 0 / 0.1)", // (default)
          large: "0 4px 6px -1px rgb(0 0 0 / 0.1), 0 2px 4px -2px rgb(0 0 0 / 0.1)",
        },
        borderWidth: {
          small: "0.5px",
          medium: "1px", // (default)
          large: "2px",
        },
      },
      themes: {
        light: {
          colors: {
            primary: {
              DEFAULT: "#3984E8",
              100: "#F0F9FF",
              200: "#BFE2FF",
              300: "#1890FF",
              400: "#96C3FF",
              500: "#ECF6FF",
              600: "#0679FF",
              700: "#3984E8",
              800: "#0165FF",
            },
            secondary: {
              500: "#F3611C",
            },
            alert: {
              600: "#F59B2F",
            },
            primaku: {
              grey: {
                50: "#E5E5E5",
                100: "#252525",
                200: "#A3A3A3",
                300: "#7C7D7F",
                400: "#BEBFC3",
                500: "#6B7280",
                600: "#494949",
                700: "#525252",
                800: "#ECEDF0",
              },
              black: {
                500: "#171717",
              },
              green: {
                50: "#EBFCCB",
                100: "#DCFCE7",
                200: "#EBF9F6",
                300: "#2FBB9B",
                400: "#25AC8D",
                500: "#166534",
                600: "#22C55E",
                700: "#3D6212",
              },
              red: {
                50: "#FFFCFC",
                100: "#FEE2E2",
                200: "#FEE9D6",
                300: "#FF6A55",
                500: "#E44712",
                600: "#ED5E5E",
                800: "#991B1B",
              },
              neutral: {
                100: "#FAFAFA",
                200: "#E5E5E5",
                300: "#F5F5F5",
                400: "#D4D4D4",
                500: "#737373",
                600: "#BFBFBF",
                700: "#404040",
              },
              blue: {
                50: "#EFF6FF",
                100: "#DBEAFE",
                500: "#1E40AF",
                600: "#0679FF",
              },
              yellow: {
                100: "#FEF9C3",
                500: "#854D0E",
              },
              orange: {
                500: "#EA9036",
              },
              purple: {
                100: "#F5E9FE",
                700: "#7424A5",
              },
            },
            outline: {
              100: "#E2E6EA",
              200: "#ECEDF0",
            },
          },
        },
      },
    }),
  ],
};
export default config;
